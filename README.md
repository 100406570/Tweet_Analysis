# ANÁLISIS DE LA EVOLUCIÓN DEL BITCOIN MEDIANTE TWEETS

**OBJETIVO DEL PROYECTO**

El principal objetivo del proyecto consiste en ser capaces de relizar un análisis de tweets de forma que seamos capaces deducir determinados datos. Estos datos obtenidos pueden ser utilizados posteriormente, de forma que puedan ser aplicados a la predicción de tendencias y estimaciones sobre las criptomonedas. En nuestro caso, vamos a centrarnos concretamente en el Bitcoin, dado que es la criptomoneda más relevante y por lo tanto habrá más gente tweeteando sobre ella. El hastag que utilizaremos para el análisis es #BTC y el idioma el inglés dado que existirá más abundancia. También podrían utilizarse otros hastag o idiomas siempre y cuando el analizador de sentimientos lo permita.


**HERRAMIENTAS UTILIZADAS**

Para el desarrollo del proyecto se han utilizado las siguientes herramientas:

- El entorno de desarrollo obtenido ha sido Google Colab y el lenguaje Python.

- Registro como desarrollador en Twitter. De esta forma obtenemos las clave de la API con las que podremos acceder a obtener los tweets en streaming. Concretamente Tweepy, dado que es para Python.

- Pandas para guardar los datos en un dataframe.

- Sentiment Vader para el analisis de sentimientos.


**DATOS OBTENIDOS**

Tras ejecutar el código con el idioma en inglés y el hastag #BTC hemos obtenido los resultados que se mostrarán a continuación. Para ello se han analizado datos durante 15 minutos en tres días diferentes, de forma que podamos observar cierta evolución. A continuación se muestran las estadísticas y datos obtenidos.

<details><summary>DÍA 1</summary>

| SENTIMIENTO | NÚMERO DE TWEETS |
| ------ | ------ |
| Positivo | 330 |
| Neutro | 199 |
| Negativo | 55 |
| Total | 584 |

![Porcentajes día 1](Day1.png)
</details>

<details><summary>DÍA 2</summary>

| SENTIMIENTO | NÚMERO DE TWEETS |
| ------ | ------ |
| Positivo | 245 |
| Neutro | 237 |
| Negativo | 43 |
| Total | 525 |

![Porcentajes día 2](Day2.png)
</details>

<details><summary>DÍA 3</summary>

| SENTIMIENTO | NÚMERO DE TWEETS |
| ------ | ------ |
| Positivo | 218 |
| Neutro | 195 |
| Negativo | 32 |
| Total | 445 |

![Porcentajes día 3](Day3.png)
</details>

- RESULTADOS FINALES

| SENTIMIENTO | DÍA 1 | DÍA 2 | DÍA 3 |
| ------ | ------ | ------ | ------ |
| Positivo | 330 | 245 | 218 |
| Neutro | 199 | 237 | 195 |
| Negativo | 55 | 43 | 32 |
| Total | 584 | 525 | 445 |

![Porcentajes totales](PercentageTotal.png)

![Evolución de sentimientos durante los tres días](PercentageDays.png)

![Número total de tweets durante los tres días](TotalEvolution.png)


**CONCLUSIÓN**

Como se puede observar en los resultados, los sentimientos son en su mayoría positivos, de forma que podríamos observar cierta satisfacción entre los usuarios lo que significaría, a grandes rasgos, que el Bitcoin está a la alza. También se puede observar que el número total de publicaciones es descendente, lo que puede significar que hemos pasado un punto de importancia en el que se publicó más de lo normal y que el número de tweets se está normalizando.

Mediante este método podríamos obtener datos de variables muy diferentes de tal forma que podríamos ser capaces de estimar con mucha más precisión e incluso podríamos entrenar un modelo de inteligencia artificial de forma que sea capaz de predecir tendencias y de esta forma nos podamos anticipar a las subidas y bajadas.
